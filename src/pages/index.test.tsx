import { render, screen } from "@testing-library/react";
import Home from './index';

describe ("<Home />", () =>{
    it("Teste se existe \"get started\" no documento", () => {
        const {container} = render( <Home />)
        expect(screen.getByText('Get started')).toBeInTheDocument()
       
    })
});
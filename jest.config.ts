import type {Config} from 'jest';

const config: Config = {
  setupFilesAfterEnv: ['<rootDir>/jest-setup.ts'],
  testEnvironment: "jsdom",
  testPathIgnorePatterns: ["/node_modules/", "/.next"],
  collectCoverage: true,
  collectCoverageFrom: ["src/**/*.ts(x)"],
};

export default config;